#!/usr/bin/env nextflow

include { manifest_to_raw_fqs } from '../preproc/preproc.nf'
include { raw_fqs_to_procd_fqs } from '../preproc/preproc.nf'

include { abra2_rna } from '../abra2/abra2.nf'
include { bedtools_intersect } from '../bedtools/bedtools.nf'
include { gffread_make_tx_fa } from '../gffread/gffread.nf'

include { kallisto_index }     from '../kallisto/kallisto.nf'
include { kallisto_quant }     from '../kallisto/kallisto.nf'
include { salmon_index }       from '../salmon/salmon.nf'
include { salmon_aln_quant }   from '../salmon/salmon.nf'
include { salmon_map_quant }   from '../salmon/salmon.nf'

include { bbmap_index }         from '../bbmap/bbmap.nf'
include { bbmap }           from '../bbmap/bbmap.nf'

include { star_index }         from '../star/star.nf'
include { star_map }           from '../star/star.nf'

include { samtools_faidx }     from '../samtools/samtools.nf'
include { samtools_index }     from '../samtools/samtools.nf'
include { picard_create_seq_dict } from '../picard2/picard2.nf'

include { clean_work_files as clean_trimmed_fastqs } from '../utilities/utilities.nf'

workflow manifest_to_transcript_counts {
// require:
//   params.rna_quant$manifest_to_transcript_counts$fq_trim_tool
//   params.rna_quant$manifest_to_transcript_counts$fq_trim_tool_parameters
//   params.rna_quant$manifest_to_transcript_counts$tx_quant_tool
//   params.rna_quant$manifest_to_transcript_counts$tx_quant_tool_parameters
//   params.rna_quant$manifest_to_transcript_counts$rna_ref
//   MANIFEST
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    tx_quant_tool
    tx_quant_tool_parameters
    rna_ref
    manifest
  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_transcript_counts(
      fq_trim_tool,
      fq_trim_tool_parameters,
      tx_quant_tool,
      tx_quant_tool_parameters,
      rna_ref,
      manifest_to_raw_fqs.out.fqs)
  emit:
    fqs = manifest_to_raw_fqs.out.fqs
    procd_fqs = raw_fqs_to_transcript_counts.out.procd_fqs
    quants = raw_fqs_to_transcript_counts.out.quants
}

workflow raw_fqs_to_transcript_counts {
// require:
//   params.rna_quant$raw_fqs_to_transcript_counts$fq_trim_tool
//   params.rna_quant$raw_fqs_to_transcript_counts$fq_trim_tool_parameters
//   params.rna_quant$raw_fqs_to_transcript_counts$tx_quant_tool
//   params.rna_quant$raw_fqs_to_transcript_counts$tx_quant_tool_parameters
//   params.rna_quant$raw_fqs_to_transcript_counts$rna_ref
//   FQS
  take:
    fq_trim_tool
    fq_trim_tool_parameters
    tx_quant_tool
    tx_quant_tool_parameters
    rna_ref
    fqs
  main:
    raw_fqs_to_procd_fqs(
      fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
    procd_fqs_to_transcript_counts(
      tx_quant_tool,
      tx_quant_tool_parameters,
      rna_ref,
      raw_fqs_to_procd_fqs.out.procd_fqs)
  emit:
    procd_fqs = raw_fqs_to_procd_fqs.out.procd_fqs
    quants = procd_fqs_to_transcript_counts.out.quants
}

workflow procd_fqs_to_transcript_counts {
// require:
//   params.rna_quant$manifest_to_transcript_counts$tx_quant_tool
//   params.rna_quant$manifest_to_transcript_counts$tx_quant_tool_parameters
//   params.rna_quant$manifest_to_transcript_counts$rna_ref
//   PROCD_FQS
  take:
    tx_quant_tool
    tx_quant_tool_parameters
    rna_ref
    procd_fqs
  main:
    tx_quant_tool_parameters = Eval.me(tx_quant_tool_parameters)
    if( tx_quant_tool =~ /salmon/ ) {
      salmon_parameters = tx_quant_tool_parameters['salmon'] ? tx_quant_tool_parameters['salmon'] : ''
      salmon_index_parameters = tx_quant_tool_parameters['salmon_index'] ? tx_quant_tool_parameters['salmon_index'] : ''
      salmon_index(
        rna_ref,
        salmon_index_parameters)
      salmon_map_quant(
        procd_fqs,
        salmon_index.out.idx_files,
        salmon_parameters)
      salmon_map_quant.out.quants.set{ quants }
    }
    if( tx_quant_tool =~ /kallisto/ ) {
      kallisto_parameters = tx_quant_tool_parameters['kallisto'] ? tx_quant_tool_parameters['kallisto'] : ''
      kallisto_index_parameters = tx_quant_tool_parameters['kallisto_index'] ? tx_quant_tool_parameters['kallisto_index'] : ''
      kallisto_index(
        rna_ref,
        kallisto_index_parameters)
      kallisto_quant(
        procd_fqs,
        kallisto_index.out.idx_files,
        kallisto_parameters)
      kallisto_quant.out.quants.set{ quants }
    }
    procd_fqs
      .concat(quants)
      .groupTuple(by: [0, 1, 2], size: 2)
      .flatten()
      .filter{ it =~ /trimmed.fastq.gz$|trimmed.fq.gz$/ }
      .set { procd_fqs_done_signal }
    clean_trimmed_fastqs(
      procd_fqs_done_signal)
  emit:
    quants
}

workflow alns_to_transcript_counts {
// require:
//   ALNS
//   params.rna_quant$alns_to_transcript_counts$rna_ref
//   params.rna_quant$alns_to_transcript_counts$gtf
//   params.rna_quant$alns_to_transcript_counts$tx_quant_tool
//   params.rna_quant$alns_to_transcript_counts$tx_quant_tool_parameters
  take:
    alns
    rna_ref
    gtf
    tx_quant_tool
    tx_quant_tool_parameters
  main:


    tx_quant_tool_parameters = Eval.me(tx_quant_tool_parameters)
    if( tx_quant_tool =~ /salmon/ ) {
      salmon_parameters = tx_quant_tool_parameters['salmon'] ? tx_quant_tool_parameters['salmon'] : ''
      gffread_make_tx_fa(
        rna_ref,
        gtf)
      gffread_make_tx_fa.out.tx_fa.set{ salmon_ref }
      salmon_aln_quant(
        alns,
        salmon_ref,
        salmon_parameters)
      salmon_aln_quant.out.quants.set{ quants }
    }
  emit:
    quants
}


workflow make_ancillary_index_files {
  take:
    ref
  main:
    samtools_faidx(
      ref,
      '')
    picard_create_seq_dict(
      ref,
      '')
    //Can't join since the ref symlinks are different.
    samtools_faidx.out.faidx_file
      .concat(picard_create_seq_dict.out.dict_file)
      .collect().map{ [it[0], it[1], it[3]] }
      .set{ collective_idx_files }
  emit:
    collective_idx_files
}
